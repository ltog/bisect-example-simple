# bisect-example-simple

A simple example to test the Git bisect feature

Question: In which commit was the error (=red background visible in `index.html`) introduced?

## How to use

1. Make sure your working directory is clean, i.e. there are no uncommited changes (`git status`).
2. Use `git log` or `git log --abbrev-commit` to find hashes of a working and a broken state. (Here, e.g: working=2e658cf and broken=c727dfb)
3. Start bisecting with the command `git bisect start $badcommit $goodcommit`, here `git bisect start c727dfb 2e658cf`. This will checkout a state between the two commits.
4. Tell git whether the commit is good or bad with either `git bisect good` or `git bisect bad`. This will checkout a new state. In our case a bad state has a red background.
5. Repeat previous step until git reports: `<commit> is the first bad commit`
6. Reset the bisecting system: `git bisect reset` (you can also use this to abort a running bisection) This will reset your HEAD to its previous position.
7. Show the culprit: `git diff $hash^..$hash` (`^` means the previous commit)
